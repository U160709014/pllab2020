#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define REFEREE_SIZE 6
#define MAXCHAR 1000
#define SKATER_SIZE 4

struct Skater{
    char* name;
    char* surname;
    double referees[REFEREE_SIZE];
    double avg;
};

double calculateScore(struct Skater* skater){
    int i;
    double sum = 0,min = 100000,max = -1;
    for(i = 0;i < REFEREE_SIZE;i++){
        if(min > (*skater).referees[i]) min = (*skater).referees[i];
        if(max < (*skater).referees[i]) max = (*skater).referees[i];
        sum += (*skater).referees[i];
    }
    double avg = (sum-min-max) / (REFEREE_SIZE);
    (*skater).avg = avg;
    return avg;
}

int loadSkaters(struct Skater skaters[],char* filename){
    FILE *fp;
    char str[MAXCHAR];

    fp = fopen(filename, "r");
    if (fp == NULL){
        printf("Could not open file %s",filename);
        return -1;
    }
    int skater_counter = 0;
    struct Skater winner;
    winner.avg = 0;
    while (fgets(str,MAXCHAR, fp) != NULL){
        struct Skater current_skater;
        loadOneSkater(&current_skater,str);
        calculateScore(&current_skater);
        if(winner.avg < current_skater.avg){
            copySkater(&winner,&current_skater);
        }
        printf("*Skater %d: %s %s*\n",skater_counter+1,current_skater.name,current_skater.surname);
        printf("Average point of Skater %d is: %.2lf\n",skater_counter+1,current_skater.avg);
        copySkater(&skaters[skater_counter],&current_skater);
        skater_counter++;
    }
    fclose(fp);
    printf("\nLoading complete\n");
    printf("File is closed.\n");

    printf("\nWinner of the Skating Cahampionship is:\n");
    printf("%s %s with %.2lf points in total.\n",winner.name,winner.surname,winner.avg);


}

void loadOneSkater(struct Skater* skater,char* skaterStr){
    char * token = strtok(skaterStr, " ");
    char* skater_values[8];
    int counter = 0;
    while( token != NULL ) {
        skater_values[counter] = token;
        counter++;
        token = strtok(NULL, " ");
    }
    (*skater).name = skater_values[0];
    (*skater).surname = skater_values[1];
    int i;
    for(i=2;i<8;i++){
        (*skater).referees[i-2] = atoi(skater_values[i]);
    }
}

void copySkater(struct Skater* s1,struct Skater* s2){
    (*s1).name = strdup((*s2).name);
    (*s1).surname = strdup((*s2).surname);
    (*s1).avg = (*s2).avg;
    int i;
    for(i = 0;i < REFEREE_SIZE;i++){
        (*s1).referees[i] = (*s2).referees[i];
    }
}

int main()
{
    struct Skater skaters[SKATER_SIZE];
    char filename[30];
    char filepath[30] = ".\\";

    printf("Enter the filename to read: ");
    scanf("%[^\n]%*c",filename);
    strcat(filepath,filename);
    loadSkaters(skaters,filename);
    return 0;
}
